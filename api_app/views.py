from django.shortcuts import render
from django.http import HttpResponse
from django.views import View
from django.http import JsonResponse
import json
from .models import user


def index(request):
    data= json.loads(request.body.decode('utf-8'))
    client_name = data["nombreCliente"]["nombre"]
    last_name = data["nombreCliente"]["apellido"]
    phone = data.get("telefono")
    age = data.get("edad")

    if int(age) < 18:
        return JsonResponse("Error in the age", status=400, safe=False)
    if len(phone) <10 or len(phone) >10:
        return JsonResponse("Error in the phone leng", status=400, safe=False)
    data = { 
        "message" :"Success"
    }
    u = user(client_name = client_name, last_name = last_name, phone = phone, age = age)
    u.save()
    tmp = u.id
    if type(tmp) == "string":
        return JsonResponse("Error", status=400, safe=False)
    else:
        return JsonResponse(data, status=201, safe=False)


def get1(request):
    i =0
    arrayResult = []
    u = user.objects.raw("SELECT * FROM api_app_user WHERE 1")
    for users in u:
        stringRes = "{nombreCliente: { nombre: "+ users.client_name + ", appellido : "+ users.last_name + "} , telefono : "+ str(users.phone) + ", edad: "+ str(users.age) + "}"
        arrayResult.append(stringRes)
        i+=1
    return JsonResponse(arrayResult, safe=False)



def delete1(request):
    data= json.loads(request.body.decode('utf-8'))
    phone = data["telefono"]
    user.objects.filter(phone=phone).delete()
    """ u = user.objects.raw("DELETE FROM api_app_user WHERE phone like " + telephone) """
    return JsonResponse("Done", status=201, safe=False)


def update1(request):
    data= json.loads(request.body.decode('utf-8'))
    phone = data.get("telefono")
    objectModel = user.objects.get(phone=phone)

    objectModel.client_name = data["nombreCliente"]["nombre"]
    objectModel.last_name = data["nombreCliente"]["apellido"]
    objectModel.phone = data.get("telefono")
    objectModel.age = data.get("edad")
    objectModel.save()
    data = { 
        "message" :"Success"
    }
    return JsonResponse(data, status=201, safe=False)