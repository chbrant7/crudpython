from django.db import models


class user(models.Model): 
    client_name = models.CharField(max_length = 50)
    last_name = models.CharField(max_length = 50)
    phone = models.IntegerField(unique=True)
    age = models.IntegerField()
# Create your models here.
